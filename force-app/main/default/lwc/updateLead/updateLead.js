import { LightningElement, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class UpdateLead extends LightningElement {
  @api recordId;
  handleOnSuccess(event) {
    // showing success messages
    const closeModal = new CustomEvent("close", {
      detail: {
        close: "true"
      }
    });
    this.dispatchEvent(closeModal);
    this.dispatchEvent(
      new ShowToastEvent({
        title: "Success!!",
        message: " Contact updated Successfully!!.",
        variant: "success"
      })
    );
  }
}
