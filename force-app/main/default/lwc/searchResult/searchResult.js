import { LightningElement, wire, track } from "lwc";
import { CurrentPageReference } from "lightning/navigation";
import findLeads from "@salesforce/apex/LeadController.findLeads";
import { registerListener, unregisterAllListeners } from "c/pubsub";
import { refreshApex } from "@salesforce/apex";

const actions = [{ label: "Edit", name: "edit" }];
const columns = [
  {
    label: "First Name",
    fieldName: "leadURL",
    type: "url",
    typeAttributes: { label: { fieldName: "firstName" }, target: "_blank" }
  },
  { label: "Last Name", fieldName: "lastname", type: "string" },
  { label: "Lead Source", fieldName: "source", type: "string" },
  { label: "Lead Status", fieldName: "status", type: "string" },
  {
    type: "action",
    typeAttributes: {
      rowActions: actions,
      menuAlignment: "right"
    }
  }
];

export default class SearchResult extends LightningElement {
  @track columns = columns;
  @track data;
  @track error;
  @track bShowModal = false;
  @track currentRecordId;
  @track isEditForm = false;
  status;
  source;
  refreshTable;

  @wire(CurrentPageReference) pageRef;

  @wire(findLeads, {
    status: "$status",
    source: "$source"
  })
  leads(result) {
    this.refreshTable = result;
    if (result.data) {
      this.data = result.data;
      this.error = undefined;
    } else if (result.error) {
      this.error = result.error;
      this.data = undefined;
    }
  }

  connectedCallback() {
    // subscribe to searchKeyChange event
    registerListener("searchKeyChange", this.handleSearchKeyChange, this);
  }

  disconnectedCallback() {
    // unsubscribe from searchKeyChange event
    unregisterAllListeners(this);
  }

  handleSearchKeyChange(searchKey) {
    this.status = searchKey.status;
    this.source = searchKey.source;
  }
  handleRowActions(event) {
    let row = event.detail.row;
    this.editCurrentRecord(row);
  }
  editCurrentRecord(currentRow) {
    // open modal box
    this.bShowModal = true;
    this.isEditForm = true;
    // assign record id to the record edit form
    this.currentRecordId = currentRow.leadId;
  }
  closeModal() {
    this.bShowModal = false;
  }
  handleChildClose(event) {
    console.log("Close event+" + JSON.stringify(event.detail));
    if (event.detail === "close") {
      closeModal();
    }
    this.bShowModal = false;
    return refreshApex(this.refreshTable);
  }
}
