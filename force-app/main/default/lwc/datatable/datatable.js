import { LightningElement, track } from 'lwc';
// import fetchDataHelper from './fetchDataHelper';

const columns = [
    { label: 'Label', fieldName: 'name' },
    { label: 'Website', fieldName: 'website', type: 'url' },
    { label: 'Phone', fieldName: 'phone', type: 'phone' },
    { label: 'Balance', fieldName: 'amount', type: 'currency' },
    { label: 'CloseAt', fieldName: 'closeAt', type: 'date' },
];
const data = [
    {
        name: 'name',
        email: 'email',
        website: 'url',
        amount: 'currency',
        phone: 'phoneNumber',
        closeAt: 'dateInFuture',
    }
];

export default class BasicDatatable extends LightningElement {
    @track data = data;
    @track columns = columns;

    // async connectedCallback() {
    //     const data = await fetchDataHelper({ amountOfRecords: 100 });
    //     // const data = {};
    //     this.data = {data: 'adsf'};
    // }
}
